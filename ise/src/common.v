`timescale 1ns / 1ps
`default_nettype none

module CALC_ADDRESS #(parameter WIDTH=80, HEIGHT=60, RAM_WIDTH=16) (
    input   wire                page,
    input   wire    [12-1:0]    line, offset,
    output  wire    [24-1:0]    address);
localparam [24-1:0] UNITWIDTH = (WIDTH+RAM_WIDTH-1)/RAM_WIDTH;
assign address =
    ({24{page}} & (HEIGHT * UNITWIDTH)) +
    line * UNITWIDTH +
    offset;
endmodule

module LFSRRND (
    input  wire             clk,
    output reg  [128-1:0]   rnd);
initial         rnd <= {8{16'h1919}};
always @(posedge clk) begin
    rnd <= {rnd[128-2:0],^{rnd[127],rnd[126],rnd[125],rnd[120]}};
end
endmodule

module PULSER #(parameter [0:0] ACTIVE=1'b1) (
    input   wire    clk,
    input   wire    fire,
    output  reg     pulse);
reg laststate = 1'b0;
always @(clk) begin
    if(fire != laststate) begin
        laststate <= fire;
        pulse <= ACTIVE;
    end else begin
        pulse <= ~ACTIVE;
    end
end
endmodule

`define c0 1'b0
`define c1 1'b1
//`define DEBUG(A) $display("%8d %5s:%3d|%s", $time, __FILE__, __LINE__, $sformatf A );
//ちーん
//`define debug(A) \
//    if(1'b0==1'b0) begin \
//        reg [128*8:1] s; \
//        $sformat(s, $sformat A ); \
//        $display("%s", s); \
//    end
