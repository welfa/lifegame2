`timescale 1ns / 1ps
`default_nettype none

//linenum_centerで指定された行+1を先読みして合計3行キャッシュする
//VGAはTOPが要らないので資源削減のため幅1にできるようにした
module LINECACHE #(parameter WIDTH=12'd800, HEIGHT=12'd600, RAM_WIDTH=8, TOP_NEEDED=1) (
    input   wire                    clk, clk_2x, reset,
    //lifegame I/F
    input   wire                    page,
    input   wire    [12-1:0]        linenum_center_raw,
    output  reg     [(WIDTH-1)*TOP_NEEDED:0]    top,
    output  reg     [WIDTH-1:0]     mid, btm,
    output  reg                     linerdy,
    //SRAM I/F
    output  wire    [24-1:0]        address,
    output  reg                     data_req,
    input   wire                    data_rdy,
    input   wire    [RAM_WIDTH-1:0] ram_data);
localparam [WIDTH-1:0]
    RESETLINE = {WIDTH{1'bX}};

localparam [11:0]   LTOP = 12'd0, LBTM = HEIGHT-12'd1;
reg     [12-1:0]    readline;// = 12'b0;
reg     [12-1:0]    readx;// = 12'b0;
reg     [12-1:0]    lastlinenum;// = 12'b0;

localparam [2:0]
    SNOP    = 3'b001,
    SREAD   = 3'b010,
    SWAIT   = 3'b100;
reg [3:0]   state = SNOP;

//page flip exception
reg             lastpage;
reg  [12-1:0]   offset;
wire [12-1:0]   linenum_center =
    offset==12'd0 ? linenum_center_raw : HEIGHT-offset;
wire            pageflipped = page ^ lastpage;

CALC_ADDRESS #(WIDTH,HEIGHT,RAM_WIDTH) ca(page,readline,readx,address);

reg     requpreq;// = 1'b0;
wire    b_requpreq;// = 1'b0;
//BUFG    brur(.I(requpreq),.O(b_requpreq));
assign  b_requpreq = requpreq;
//多分要らないけど遅いと言われるので一応

always @(posedge clk_2x, posedge reset) begin
    if(reset) begin
        data_req    <= 1'b0;
    end else begin
        data_req <=
            data_rdy ? 1'b0 :
            b_requpreq ? 1'b1 : data_req;
    end
end

always @(posedge clk, posedge reset) begin
    if(reset) begin
        //  top         <= RESETLINE;
        //  mid         <= RESETLINE;
        //  btm         <= RESETLINE;
        linerdy     <= 1'b0;
        readline    <= 12'b0;
        readx       <= 12'b0;
        lastlinenum <= 12'hFFF;
        state       <= SNOP;
        requpreq    <= 1'b0;
        lastpage    <= 1'b0;
        offset      <= 1'b0;
    end else begin
        case (state)
        SNOP: begin
            if(pageflipped) begin
                //pageが切り替わった時の特殊動作
                offset      <= 12'd2;
                lastpage    <= page;
                lastlinenum <= HEIGHT;  //ありえない値
                if(linerdy!=1'b0) begin
                    //$display($time, " deassert linerdy");
                end
                linerdy     <= 1'b0;
                //stateはそのまま
            end else begin
                if(|(lastlinenum ^ linenum_center)) begin
                    //linenumに変化があったら実行
                    lastlinenum <= linenum_center;
                    //linenumに依らずこのシフトで合ってる
                    top <= mid;
                    mid <= btm;
                    if(linerdy!=1'b0) begin
                        //$display($time, " deassert linerdy");
                    end
                    linerdy <= 1'b0;
                    readx <= 12'b0;
                    state <= SREAD;
                    case (linenum_center)
                    LBTM:       readline <= 12'd0;                  //btmに一番上を入れる
                    default:    readline <= linenum_center+12'd1;   //btmに次の行を入れる
                    endcase
                end
            end
        end
        SREAD: begin
            //SRAMを叩いてlineを埋めていく
            //SWAITで+1してからこっちに来るので比較は<=
            if(!pageflipped && readx<=(WIDTH-12'b1)/RAM_WIDTH) begin
                //flipされてた時は続きを読む必要がないのでココには入らない
                //$display($time, " read line %xh to btm readx=%xh, address=%xh", readline, readx, address);
                requpreq <= 1'b1;
                state <= SWAIT;
            end else begin
                readx <= 12'd0; //いらない
                if(offset==12'd0) begin
                    //$display($time, " assert linerdy");
                    linerdy <= 1'b1;
                end
                state <= SNOP;
                //pageflipの後始末
                if(offset!=12'd0) begin offset <= offset-12'd1; end
            end
        end
        SWAIT: begin
            //データ街
            if(data_rdy) begin
                //データ北
                //$display($time, " address=%xh's data=%b", address, ram_data);
                requpreq <= 1'b0;
                btm[readx*RAM_WIDTH +: RAM_WIDTH] <= ram_data;
                readx <= readx + 12'b1;
                state <= SREAD;
            end
        end
        default: begin
            linerdy <= 1'bX;   //エラー
            $display($time, " state error @linecache");
        end
        endcase
    end
end

endmodule
