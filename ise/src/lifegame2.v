`timescale 1ns / 1ps
`default_nettype none

module LIFEGAME2 #(parameter[12-1:0] WIDTH=16'd800, HEIGHT=16'd600) (
    input   wire        clk_orig,
    output  wire        clk_sram,
    input   wire        reset_sw_n,
    input   wire [5:0]  sw,
    //vga I/F
    output  wire [3:0]  vga_r,  vga_g,  vga_b,  //  VGA RGB
    output  wire        vga_hs, vga_vs,         //  VGA H/V sync
    //LED
    output  wire        debug_out,
    //SRAM
    output  wire [17:0] sram_addr,
    output  wire [15:0] sram_to,
    input   wire [15:0] sram_from,
    output  wire        sram_we_n, sram_oe_n,
                        sram_ub_n, sram_lb_n, sram_ce_n);

wire    clk, clk_n, clk_2x, v_clk, v_clk_2x, clk32, clk50, clk50_n, clk100;
wire    dcm_locked;

//起動直後リセット状態にする
reg  [16-1:0]    wake = 16'h1;
wire            reset_c = ~reset_sw_n | (|wake);
always @(posedge clk50) begin
    if(|wake) begin
        $display("wake:%b", wake);
    end
    wake <= {wake[16-2:0], 1'b0};
end

//クロック
//for implementation
wire clk50_bridge_dcm, dcm_locked_50m;
DCM50M dcm50m(
    clk_orig,
    ~reset_sw_n,
    clk50_bridge_dcm,
    ,
    clk32,
    dcm_locked_50m
);
DCMx2 dcmx2(
    clk50_bridge_dcm,
    ~reset_sw_n | ~dcm_locked_50m,
    clk50,
    clk100,
    clk50_n,
    dcm_locked
);
//for simulation
//assign clk100= clk_orig;
//reg clk50_divreg=1'b0;
//always @(posedge clk100) clk50_divreg <= ~clk50_divreg;
//assign clk50 = clk50_divreg;
//assign clk32 = clk50;
//assign dcm_locked = 1'b1;

assign  clk     = clk50;
assign  clk_2x  = clk100;
assign  clk_n   = clk50_n;
assign  v_clk   = clk50;
assign  v_clk_2x= clk100;
assign  clk_sram= clk50;

//resetはposedgeで見ているので最初にlowが出るようにする  & (~|wake[1:0])
wire    reset       = (reset_c | ~dcm_locked) & (~|wake[1:0]);
always @(reset) begin
    if(reset)   $display("reset");
    else        $display("release reset");
end

localparam
    ADDRWIDTH   = 18,
    DATAWIDTH   = 16;
wire [12-1:0]   _hcounter, _vcounter;
wire            reading, rgbmask;

assign  debug_out   = v_clk & rgbmask;

VGA #(WIDTH*2,HEIGHT*2) vga( //640x480
    v_clk,      reset,
    vga_hs,     vga_vs,
    reading,    rgbmask,
    _hcounter,  _vcounter);
// 1/2
wire [12-1:0]
    hcounter = {1'b0, _hcounter[11:1]},
    vcounter = {1'b0, _vcounter[11:1]};

wire start, rw;
wire [ADDRWIDTH-1:0]    addr_in;
wire [DATAWIDTH-1:0]    data_read;
wire [DATAWIDTH-1:0]    data_write;
SRAMCTRL #(ADDRWIDTH, DATAWIDTH) sramctrl(
    //inner I/F
    clk_n,      reset,
    start,      rw,
    addr_in,
    data_read,
    data_write,
    //SRAM I/F
    sram_addr,
    sram_we_n,      sram_oe_n,
    sram_ce_n,      sram_ub_n,      sram_lb_n,
    sram_to,
    sram_from
);

wire [ADDRWIDTH-1:0]    lg_R_address;
wire [DATAWIDTH-1:0]    lg_R_ram_data;
wire                    lg_R_data_req,  lg_R_data_rdy;
reg  [ADDRWIDTH-1:0]    lg_W_address;
reg  [DATAWIDTH-1:0]    lg_W_data;// = {DATAWIDTH{1'b0}};
reg                     lg_W_write;// = 1'b0;
wire                    lg_W_written;
wire [ADDRWIDTH-1:0]    v_address;
wire [DATAWIDTH-1:0]    v_ram_data;
wire                    v_data_req,     v_data_rdy;
SRAMPROXY #(ADDRWIDTH, DATAWIDTH) sramproxy(
    clk, reset,
    lg_R_address,
    lg_R_data_req,
    lg_R_data_rdy,
    lg_R_ram_data,
    lg_W_address,
    lg_W_write,   //reqはrdyが上がった瞬間下げる
    lg_W_data,
    lg_W_written,
    v_address,
    v_data_req,   //reqはrdyが上がった瞬間下げる
    v_data_rdy,
    v_ram_data,
    //SRAM I/F
    start,
    rw,
    addr_in,
    data_read,
    data_write);

//初期配置用乱数生成
wire    [128-1:0]       rnd;
LFSRRND lfsr(clk, rnd);
wire    [128*10-1:0]    mrnd={10{rnd}};

wire    [WIDTH-1:0]
    lg_top, lg_mid, lg_btm, lg_next;
reg     loadrnd = 1'b1;
wire    [WIDTH-1:0]
    ns_top = loadrnd ? mrnd[   0 +: WIDTH] : lg_top,
    ns_mid = loadrnd ? mrnd[1200 -: WIDTH] : lg_mid,
    ns_btm = loadrnd ? mrnd[  60 +: WIDTH] : lg_btm;

//次状態生成
localparam
    NSDIV = 1;
reg     nsstart;
NEXTSTEP #(WIDTH, NSDIV) ns(
    clk_2x, reset, nsstart, ns_top, ns_mid, ns_btm, lg_next);

//メイン処理実行ステートマシン
//linenum変える
//読み込み待つ
//次行計算待ち
//左から順に書き出す
//foreach
//  アドレスとデータ入れてwrite挙げる
//  written待つ(立ったら即write下げる)
//h方向最後まで書いたら次行(行num変えてstate0)へ
//v方向最後まで書いたらvgaのvcountがデカくなるのを待ってpageを反転，state0へ
localparam [7:0]
    US_WAITCNGNM    = 8'h00,
    US_WAITREAD     = 8'h10,
    US_WAITCALC_BASE= 8'h20,
    US_WAITCALC_X   = 8'h2X,
    US_WRITE        = 8'h30,
    US_WAITWRITE    = 8'h40,
    US_WAITVSYNC    = 8'h50;
localparam [12-1:0]
    RESETHVC = 12'b0;
reg  [12-1:0]   lg_hcounter,// = RESETHVC,
                lg_vcounter;// = RESETHVC;
reg             page;// = 1'b0;    //page:stable, ~page:unstable
reg  [7:0]      updatestate;// = US_WAITREAD;

wire [ADDRWIDTH-1:0]    lg_W_address_next;
CALC_ADDRESS #(WIDTH,HEIGHT,DATAWIDTH) ca(
    ~page,lg_vcounter,lg_hcounter,lg_W_address_next);

reg     flipclearreq;
reg     allowflip;
always @(posedge v_clk_2x, posedge reset) begin
    if(reset) begin
        allowflip <= 1'b1;
    end else begin
        allowflip <=
            vcounter==12'b0 && hcounter==12'b0  ? 1'b1 :
            flipclearreq                        ? 1'b0 : allowflip;
    end
end

reg     wrequpreq;// = 1'b0;
wire    b_wrequpreq;
//BUFG    bwrur(.I(wrequpreq),.O(b_wrequpreq));
assign  b_wrequpreq = wrequpreq;
reg     lastlgww, lastbrur;
always @(posedge clk_2x,/*posedge lg_W_written, posedge b_wrequpreq,*/  posedge reset) begin
    if(reset) begin
        lg_W_write  <= 1'b0;
        lastlgww    <= 1'b0;
        lastbrur    <= 1'b0;
    end else begin
        if(((~lastlgww) && lg_W_written) || ((~lastbrur) && b_wrequpreq)) begin
            lg_W_write <=
                b_wrequpreq ? 1'b1 :
                lg_W_written ? 1'b0 : lg_W_write;
        end
        if(lg_W_written!==1'bx) //for simulation if state
            lastlgww <= lg_W_written;
        if(b_wrequpreq!==1'bx)
            lastbrur <= b_wrequpreq;
    end
end

wire    lg_linerdy;
always @(posedge clk, posedge reset) begin
    if(reset) begin
        loadrnd     <= 1'b1;
        lg_hcounter <= 12'b0;
        lg_vcounter <= 12'b0;
        //lg_W_data   <= {DATAWIDTH{1'b0}};
        page        <= 1'b0;    //page:stable, ~page:unstable
        updatestate <= US_WAITREAD;
        flipclearreq<= 1'b0;
        nsstart <= 1'b0;
    end else begin
        if(page) begin loadrnd <= 1'b0; end
        //$display($time, " current Ustate:%d", updatestate);
        casex(updatestate)
        US_WAITCNGNM: begin
            flipclearreq <= 1'b0;
            if(~lg_linerdy) updatestate <= US_WAITREAD;
        end
        US_WAITREAD: begin
            //$display($time, " WAITREAD rdy:%b", lg_linerdy);
            if(lg_linerdy) begin
                updatestate <= US_WAITCALC_BASE + (NSDIV+1)/2 - 1;
                nsstart     <= 1'b1;
            end
        end
        US_WAITCALC_X: begin
            nsstart     <= 1'b0;
            if(updatestate==US_WAITCALC_BASE) begin
                updatestate <= US_WRITE;
            end else begin
                updatestate <= updatestate - 8'h1;
            end
        end
        US_WRITE: begin
            if(~lg_W_written) begin //busyでなければ
                //右側からDATAWIDTHビットづつ書いていく
                lg_W_data   <= lg_next[lg_hcounter*DATAWIDTH +: DATAWIDTH];
                //ステート<-書き待ち更新， アドレス更新
                updatestate <= US_WAITWRITE;
                lg_W_address<= lg_W_address_next;
                lg_hcounter <= (lg_hcounter<(WIDTH-1)/DATAWIDTH) ? lg_hcounter+12'b1 : 12'b0;
                lg_vcounter <= (lg_hcounter<(WIDTH-1)/DATAWIDTH) ? lg_vcounter :
                               (lg_vcounter<(HEIGHT-1)) ? lg_vcounter+12'b1 : 12'b0;
                wrequpreq   <= 1'b1;
            end
        end
        US_WAITWRITE: begin
            wrequpreq <= 1'b0;  //"書けよ命令上げろよreg"を下ろす
            if(lg_W_written) begin  //書けたか
                if(lg_vcounter==12'b0 && lg_hcounter==12'b0) begin
                    //終端まで行ったらpage切り替えしてからwaitreadへ
                    updatestate <= US_WAITVSYNC;
                end else if(lg_hcounter==12'b0) begin
                    //行が変わったらwaitcngnumへ
                    updatestate <= US_WAITCNGNM;
                end else begin
                    //まだ1行書き終わって無かったら次行を書く
                    updatestate <= US_WRITE;
                end
            end
        end
        US_WAITVSYNC: begin
            //allowflipを無視するとvisible area外で2回以上計算できる
            //ハードの都合で2回以上計算できるとは言っていない
            if(!reading /*&& allowflip*/) begin
                $display("flip page: %b -> %b", page, ~page);
                page <= ~page;
                updatestate <= US_WAITCNGNM;
                flipclearreq <= 1'b1;
            end
        end
        default: $display(" error state 1919364");
        endcase
    end
end

//更新用3ラインキャッシュ
LINECACHE #(WIDTH, HEIGHT, DATAWIDTH, 1) lg_cache(
    clk, clk_2x, reset,
    //lifegame I/F
    page,
    lg_vcounter,
    lg_top, lg_mid, lg_btm,  //まずは1bitだけ
    lg_linerdy,
    //SRAM I/F
    lg_R_address,
    lg_R_data_req,
    lg_R_data_rdy,
    lg_R_ram_data);
wire    [WIDTH-1:0] v_mid;
wire    [12-1:0]    v_mid_index = hcounter<WIDTH ? hcounter : 12'b0;
assign  vga_r = {4{v_mid[v_mid_index] & rgbmask}};
assign  vga_g = vga_r,
        vga_b = vga_r;

//描画用2ラインキャッシュ
wire    [12-1:0]    valid_vcounter = vcounter<HEIGHT ? vcounter : 12'b0;
LINECACHE #(WIDTH, HEIGHT, DATAWIDTH, 0) v_cache(
    clk, clk_2x, reset,
    //lifegame I/F
    page,
    valid_vcounter,
    , v_mid, ,  //まずは1bitだけ
    ,
    //SRAM I/F
    v_address,
    v_data_req,
    v_data_rdy,
    v_ram_data);

endmodule
