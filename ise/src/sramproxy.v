`timescale 1ns / 1ps
`default_nettype none

module SRAMPROXY #(parameter ADDRWIDTH=18, DATAWIDTH=16) (
    input   wire                    clk, reset,
    //lifegame R    (low priority)
    input   wire [ADDRWIDTH-1:0]    address,
    input   wire                    data_req,   //reqはrdyが上がった瞬間下げろ
    output  reg                     data_rdy,
    output  reg  [DATAWIDTH-1:0]    ram_rdata,
    //lifegame W    (low priority)
    //posedge writeでram_wdataをregに一旦格納して書き込む
    input   wire [ADDRWIDTH-1:0]    w_address,
    input   wire                    write,   //reqはrdyが上がった瞬間下げろ
    input   wire [DATAWIDTH-1:0]    ram_wdata,
    output  reg                     written,
    //vram          (high priority)
    input   wire [ADDRWIDTH-1:0]    v_address,
    input   wire                    v_data_req,   //reqはrdyが上がった瞬間下げろ
    output  reg                     v_data_rdy,
    output  reg  [DATAWIDTH-1:0]    v_ram_rdata,
    //SRAM I/F
    output  reg                     start,
    output  reg                     rw,
    output  reg  [ADDRWIDTH-1:0]    addr_in,
    input   wire [DATAWIDTH-1:0]    data_read,
    output  reg  [DATAWIDTH-1:0]    data_write);

//最上位ビットはそのステートに入る前の待ちを意味する
localparam [3:0]
    TNO     = 4'b0000,
    TRBASE  = 4'b0100,
    TRX     = 4'b01XX,
    TWBASE  = 4'b1000,
    TWX     = 4'b10XX,
    TVRBASE = 4'b1100,
    TVRX    = 4'b11XX,
    WAITNUM = 4'b0001,
    WAITST1 = 4'bXX1X,  //きたない
    WAITST2 = 4'bXXX1;
localparam
    READ = 1'b1,
    WRITE = 1'b0;
localparam [DATAWIDTH-1:0]
    INITDATA = {DATAWIDTH{1'bX}};
localparam [ADDRWIDTH-1:0]
    INITADDR = {ADDRWIDTH{1'bX}};
reg [3:0] state = TNO;

//2clockで終わらせるため，
//stateがNOなら即sram駆動，
//stateが何かしらならsram I/Fをいじらないようにする
always @(posedge clk, posedge reset) begin
    if(reset) begin
        data_rdy    <= 1'b0;
        written     <= 1'b0;
        v_data_rdy  <= 1'b0;
        //  ram_rdata   <= INITDATA;
        //  v_ram_rdata <= INITDATA;
        start       <= 1'b0;
        rw          <= 1'b1;
        //  addr_in     <= INITADDR;
        //  data_write  <= INITDATA;
        state       <= TNO;
    end else begin
        casex(state)
            TNO : begin
                //$display($time, " TNO state");
                data_rdy <= 1'b0;
                written <= 1'b0;
                v_data_rdy <= 1'b0;
                state <=
                    v_data_req  ? WAITNUM | TVRBASE :
                    write       ? WAITNUM | TWBASE :
                    data_req    ? WAITNUM | TRBASE : TNO;
                rw <= write     ? WRITE : READ;
                addr_in <=
                    v_data_req  ? v_address :
                    write       ? w_address :
                    data_req    ? address : {(ADDRWIDTH-1){1'b0}};
                data_write <=
                    write       ? ram_wdata : {(DATAWIDTH-1){1'b0}};
                //$display("v_data_req:%b, write:%b, data_req:%b", v_data_req, write, data_req);
                start <=
                    v_data_req || write || data_req ? 1'b1 : 1'b0;
            end
            WAITST1, WAITST2 : begin
                state <= state-4'b1;
            end
            TRX : begin
                //$display($time, " TR state");
                ram_rdata <= data_read;
                data_rdy <= 1'b1;
                start <= 1'b0;
                state <= TNO;
            end
            TWX : begin
                //$display("ram_wdata:%xh", data_write);
                written <= 1'b1;
                start <= 1'b0;
                state <= TNO;
            end
            TVRX: begin
                v_ram_rdata <= data_read;
                v_data_rdy <= 1'b1;
                start <= 1'b0;
                state <= TNO;
            end
            default : begin end
        endcase
    end
end

endmodule

