`timescale 1ns / 1ps
`default_nettype none

//実装algorithm:
//ビット演算（ビットボード）によるライフゲーム高速化
//Nobuhide Tsuda
//23-Dec-2012
//http://vivi.dyndns.org/tech/games/LifeGame.html
module NEXTSTEP #(parameter WIDTH=320, DIV=4) (
    input   wire                clk, reset, start,
    input   wire    [WIDTH-1:0] top, mid, btm,
    output  reg     [WIDTH-1:0] next);

//2^n分割してnクロックで次の行を出力する．
//gcd(1024,1152,768,1280)=2^7なので適当に分割しても端数がでない，すごい
localparam DIVWIDTH = WIDTH/DIV;
wire [WIDTH-1+2:0]
    t = {top[0], top, top[WIDTH-1]},
    m = {mid[0], mid, mid[WIDTH-1]},
    b = {btm[0], btm, btm[WIDTH-1]};
//3210          WIDTH=2
//oooo          [WIDTH-1+2:0]
//oo    left    [WIDTH-1+2:2]
// oo   center  [WIDTH-1+1:1]
//  oo  right   [WIDTH-1+0:0]

reg [3:0]   state;
//下記はこれと等価
//wire [DIVWIDTH-1+2:0]
//    dt = t[state*DIVWIDTH +: DIVWIDTH+2],
//    dm = m[state*DIVWIDTH +: DIVWIDTH+2],
//    db = b[state*DIVWIDTH +: DIVWIDTH+2];
wire [DIVWIDTH-1+2:0]
    t_div [DIV-1:0],
    m_div [DIV-1:0],
    b_div [DIV-1:0];

wire [DIVWIDTH-1+2:0] dt,dm,db;
//[+:]で変数を使うとめっちょ合成が遅いので定数にしてやる
genvar i;
generate for(i=0; i<DIV; i=i+1) begin: NSGENASSIGN
    assign
        t_div[i] = t[i*DIVWIDTH +: DIVWIDTH+2],
        m_div[i] = m[i*DIVWIDTH +: DIVWIDTH+2],
        b_div[i] = b[i*DIVWIDTH +: DIVWIDTH+2];
end endgenerate
assign
    dt = t_div[state],
    dm = m_div[state],
    db = b_div[state];

//0
wire [DIVWIDTH-1:0] b0_0 = dt[DIVWIDTH-1+0:0] ^ dt[DIVWIDTH-1+1:1];
wire [DIVWIDTH-1:0] b1_0 = dt[DIVWIDTH-1+0:0] & dt[DIVWIDTH-1+1:1];
wire [DIVWIDTH-1:0] b2_0 = {(DIVWIDTH-1){1'b0}};
wire [DIVWIDTH-1:0] x_0  = b0_0 & dt[DIVWIDTH-1+2:2];
//1
wire [DIVWIDTH-1:0] b0_1 = b0_0 ^ dt[DIVWIDTH-1+2:2];
wire [DIVWIDTH-1:0] b2_1 = b2_0 ^ b1_0 & x_0;
wire [DIVWIDTH-1:0] b1_1 = b1_0 ^ x_0;
wire [DIVWIDTH-1:0] x_1  = b0_1 & dm[DIVWIDTH-1+0:0];
//2
wire [DIVWIDTH-1:0] b0_2 = b0_1 ^ dm[DIVWIDTH-1+0:0];
wire [DIVWIDTH-1:0] b2_2 = b2_1 ^ b1_1 & x_1;
wire [DIVWIDTH-1:0] b1_2 = b1_1 ^ x_1;
wire [DIVWIDTH-1:0] x_2  = b0_2 & dm[DIVWIDTH-1+2:2];
//3
wire [DIVWIDTH-1:0] b0_3 = b0_2 ^ dm[DIVWIDTH-1+2:2];
wire [DIVWIDTH-1:0] b2_3 = b2_2 ^ b1_2 & x_2;
wire [DIVWIDTH-1:0] b1_3 = b1_2 ^ x_2;
wire [DIVWIDTH-1:0] x_3  = b0_3 & db[DIVWIDTH-1+0:0];
//4
wire [DIVWIDTH-1:0] b0_4 = b0_3 ^ db[DIVWIDTH-1+0:0];
wire [DIVWIDTH-1:0] b2_4 = b2_3 ^ b1_3 & x_3;
wire [DIVWIDTH-1:0] b1_4 = b1_3 ^ x_3;
wire [DIVWIDTH-1:0] x_4  = b0_4 & db[DIVWIDTH-1+1:1];
//5
wire [DIVWIDTH-1:0] b0_5 = b0_4 ^ db[DIVWIDTH-1+1:1];
wire [DIVWIDTH-1:0] b2_5 = b2_4 ^ b1_4 & x_4;
wire [DIVWIDTH-1:0] b1_5 = b1_4 ^ x_4;
wire [DIVWIDTH-1:0] x_5  = b0_5 & db[DIVWIDTH-1+2:2];
//6
wire [DIVWIDTH-1:0] b0_6 = b0_5 ^ db[DIVWIDTH-1+2:2];
wire [DIVWIDTH-1:0] b2_6 = b2_5 ^ b1_5 & x_5;
wire [DIVWIDTH-1:0] b1_6 = b1_5 ^ x_5;
wire [DIVWIDTH-1:0] x_6  = ~b2_6 & b1_6;

always @(posedge clk, posedge reset) begin
    if(reset) begin
        state <= 4'h0;
    end else begin
        if(state==4'h0) begin   //state==S_0
            if(start)       state <= state + 4'h1;
        end else begin          //state!=S_0
            if(state!=DIV-1)state <= state + 4'h1;
            else            state <= 4'h0;
        end
    end
end

//[+:]の左項に変数を使うとめっちゃコンパイルが遅い&おかしいので定数にしてやる
genvar j;
generate for(j=0; j<DIV; j=j+1) begin: NSGENSAVE
    always @(posedge clk) begin
        if(!reset) begin
            if(start || state!=4'h0 && state==j) begin
                next[j*DIVWIDTH +: DIVWIDTH]
                    <= (dm[DIVWIDTH-1+1:1] & (x_6 & ~b0_6)) | (x_6 & b0_6);
            end
        end
    end
end endgenerate

endmodule

//資源が足りないのを各ステートへの分割でナントカしようと思ったけど
//2割程度しか削減できなかった．ボツ
//    reg [WIDTH-1:0] b0, b1, b2, x;
//
//    localparam [3:0]
//        S_0 = 4'h0,
//        S_1 = 4'h1,
//        S_2 = 4'h2,
//        S_3 = 4'h3,
//        S_4 = 4'h4,
//        S_5 = 4'h5,
//        S_6 = 4'h6;
//    reg [3:0] state;
//
//    function [WIDTH-1:0] COMMONSGNL(
//        input [3:0] s);
//        COMMONSGNL = 
//            (s==S_0 ? t[WIDTH-1+0:0] : b0)
//            ^
//            (s==S_0 ? t[WIDTH-1+1:1] :
//             s==S_1 ? t[WIDTH-1+2:2] :
//             s==S_2 ? m[WIDTH-1+0:0] :
//             s==S_3 ? m[WIDTH-1+2:2] :
//             s==S_4 ? b[WIDTH-1+0:0] :
//             s==S_5 ? b[WIDTH-1+1:1] :
//                      b[WIDTH-1+2:2]);
//    endfunction
//
//    always @(posedge clk) begin
//        b0 <= COMMONSGNL(state);
//        x  <= COMMONSGNL(state) & (
//            state==S_0 ? t[WIDTH-1+2:2] :
//            state==S_1 ? m[WIDTH-1+0:0] :
//            state==S_2 ? m[WIDTH-1+2:2] :
//            state==S_3 ? b[WIDTH-1+0:0] :
//            state==S_4 ? b[WIDTH-1+1:1] :
//          /*state==S_5?*/b[WIDTH-1+2:2]);
//            //x_6は使用しないのでdont care
//        b2 <= b2 ^ (state==S_0 ? b2 : (b1 & x));
//        b1 <= state==S_0 ? t[WIDTH-1+0:0] & t[WIDTH-1+1:1] : b1 ^ x;
//
//        if(state==S_0) begin    //state==S_0
//            if(start) begin state <= S_1; end
//        end else begin          //state!=S_0
//            if(state!=S_6)  state <= state + 4'h1;
//            else            state <= S_0;
//        end
//    end
//    assign next = (m[WIDTH-1+1:1] & (~b2 & b1 & ~b0)) | (~b2 & b1 & b0);

