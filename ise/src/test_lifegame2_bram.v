`timescale 1ns / 1ps
`default_nettype none

module LIFEGAME2_BRAM #(parameter WIDTH=12'd320, HEIGHT=12'd240) ();
reg         clk;
reg         reset_sw_n;
wire [5:0]  sw;
    //vga I/F

wire        clk_sram;
wire        dbo;
wire [3:0]  vr, vg, vb;
wire        vga_hs, vga_vs;
wire [17:0] sram_addr;
wire [15:0] sram_to, sram_from;
wire        sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n;

always #5 clk <= ~clk;
always @(posedge vga_vs) begin
  //$display("\033c");
    $display("vs\n%d", $time);
end
reg nl = 1'b0;
always @(negedge dbo, negedge vga_hs) begin
    if(~vga_hs) begin
        nl <= 1'b1;
    end else if(~dbo) begin
        if(nl) begin
            nl <= 1'b0;
            $display("hs");
        end
        #1
        $write("%s",
            vr[0]===1'b1 ? "@" :
            vr[0]===1'b0 ? "." :
            vr[0]===1'bZ ? "Z" :
            vr[0]===1'bX ? "X" : "!");
    end
end

initial begin
    clk <= 1'b1;
    reset_sw_n <= 1'b1;
    #5;
    reset_sw_n <= 1'b0;
    #10;
    reset_sw_n <= 1'b1;
    //repeat(10000) @(posedge clk);
    //$finish;
end


LIFEGAME2 #(WIDTH,HEIGHT) lg2(
    clk, clk_sram, reset_sw_n, sw, vr, vg, vb, vga_hs, vga_vs, dbo,
    sram_addr,
    sram_to,
    sram_from,
    sram_we_n, sram_oe_n,
    sram_ub_n, sram_lb_n, sram_ce_n);

wire [16-1:0]   to_bram, from_bram;
assign to_bram = sram_to, sram_from = from_bram;
////assign  to_bram     = sram_we_n ? 16'hZZZZ : sram_dq;
//assign  to_bram     = sram_dq;
////assign  from_bram   = sram_we_n ? sram_dq  : 16'hZZZZ;
//assign  sram_dq = sram_we_n ? from_bram : 16'hZZZZ;
BRAM bram(clk_sram, ~reset_sw_n, ~sram_we_n, sram_addr, to_bram, from_bram);

endmodule
