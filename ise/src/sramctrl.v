`timescale 1ns / 1ps
`default_nettype none

module SRAMCTRL #(parameter ADDRWIDTH=18, DATAWIDTH=16) (
    //inner I/F
    input   wire                    clk,        reset,
    input   wire                    start,      rw,
    input   wire [ADDRWIDTH-1:0]    addr,
    output  reg  [DATAWIDTH-1:0]    rdata,
    input   wire [DATAWIDTH-1:0]    wdata,
    //SRAM I/F
    output  wire [ADDRWIDTH-1:0]    sram_addr,
    output  reg                     we_n, oe_n,
    output  wire                    ce_a_n, ub_a_n, lb_a_n,
    output  wire [DATAWIDTH-1:0]    data_to,
    input   wire [DATAWIDTH-1:0]    data_from
);
localparam [1:0]
    SIDLE   = 2'b00,
    SREAD   = 2'b01,
    SWRITE  = 2'b10;
reg [1:0] state = SIDLE;
localparam [0:0]
    TRI_DRIVE   = 1'b1,
    TRI_HIZ     = 1'b0;
reg tri_data    = TRI_HIZ;
//assign data_io  = tri_data ? wdata : {DATAWIDTH{1'bZ}};
assign data_to = wdata;
localparam [0:0]
    READ    = 1'b1,
    WRITE   = 1'b0;
assign sram_addr = addr;
assign ce_a_n=1'b0, ub_a_n=1'b0, lb_a_n=1'b0;

always @(posedge clk, posedge reset) begin
    if(reset) begin
        //rdata <= {DATAWIDTH/2{2'bZX}};
        state <= SIDLE;
        tri_data <= TRI_HIZ;
        we_n <= 1'b1;
        oe_n <= 1'b1;
    end else begin
        case(state)
        SIDLE: begin
            if(start) begin
                case(rw)
                READ: begin
                    state   <= SREAD;
                    oe_n    <= 1'b0;
                end
                WRITE: begin
                    state   <= SWRITE;
                    tri_data<= TRI_DRIVE;
                    we_n    <= 1'b0;
                end
                default: $display($time, " error state 114514");
                endcase
            end
        end
        SREAD: begin
            state   <= SIDLE;
            oe_n    <= 1'b1;
            rdata   <= data_from;//data_io;
        end
        SWRITE: begin
            //$display("wdata:%xh",wdata);
            //$display("w-data_io:%xh",data_io);
            state   <= SIDLE;
            tri_data<= TRI_HIZ;
            we_n    <= 1'b1;
        end
        default: $display($time, " error state 1919810");
        endcase
    end
end
endmodule

