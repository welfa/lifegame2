`timescale 1ns / 1ps
`default_nettype none

module TEST_LIFEGAME2 #(parameter WIDTH=12'd032, HEIGHT=12'd016) ();
reg         clk, reset_n;
wire [3:0]  vga;
wire        hs, vs, dbo;
wire [17:0] sram_addr;
wire [15:0] sram_dq;
wire [15:0] sram_to, sram_from;
wire        sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n;
LIFEGAME2 #(WIDTH,HEIGHT) lg2(
    clk, , reset_n, , vga, , , hs, vs, dbo,
    sram_addr,
    //sram_dq,
    sram_to,
    sram_from,
    sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n);
//TODO: sram_dqとto/fromつなげる
//assign  sram_to = sram_dq;
//assign  sram_dq = sram_we_n ? sram_from : 16'hZZZZ;
assign sram_from = sram_dq;
assign sram_dq = sram_we_n ? 16'hZZZZ : sram_to;
SRAM sram(sram_addr, sram_dq, sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n);

always #5 clk <= ~clk;
always @(posedge vs) begin
  //$display("\033c");
    $display("vs\n%d", $time);
end
reg nl = 1'b0;
always @(negedge dbo, negedge hs) begin
    if(~hs) begin
        nl <= 1'b1;
    end else if(~dbo) begin
        if(nl) begin
            nl <= 1'b0;
            $display("hs");
        end
        #1
        $write("%s", vga[0] ? "X" : ".");
    end
end


initial begin
    clk <= 1'b1;
    reset_n <= 1'b1;
    #5;
    reset_n <= 1'b0;
    #10;
    reset_n <= 1'b1;
    //repeat(10000) @(posedge clk);
    //$finish;
end

endmodule
