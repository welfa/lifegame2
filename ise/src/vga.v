`timescale 1ns / 1ps
`default_nettype none
//http://tinyvga.com/vga-timing/800x600@72Hz
//HEIGHT,WIDTHは本番時固定.デバッグで可変だと役に立つど
module VGA #(parameter[11:0] WIDTH=12'd800, HEIGHT=12'd600) (
        input   wire        clk,        reset,
        output  wire        hsync,      vsync,
                            reading,    rgbmask,
        output  reg [11:0]  hcounter,   vcounter);
    localparam [11:0] RESETHVC = 12'b0;

    localparam [11:0]
        WZERO   = 12'b0,
        HVA     = WIDTH,
        HFP     = 56,
        HSP     = 120,
        HBP     = 64,
        HWHOLE  = HVA + HFP + HSP + HBP,
        VVA     = HEIGHT,
        VFP     = 37,
        VSP     = 6,
        VBP     = 23,
        VWHOLE  = VVA + VFP + VSP + VBP;

    assign hsync = ~(HVA+HFP<=hcounter && hcounter<HWHOLE-HBP);
    assign vsync = ~(VVA+VFP<=vcounter && vcounter<VWHOLE-VBP);
    assign reading = WZERO<=vcounter && vcounter<VVA;
    assign rgbmask = WZERO<=hcounter && hcounter<HVA && reading;

    always @(posedge clk, posedge reset) begin
        if (reset) begin
            hcounter    <= WZERO;
            vcounter    <= WZERO;
        end else begin
            hcounter <= (hcounter < HWHOLE-12'b1) ? hcounter+12'b1 : WZERO;
            vcounter <=
                (hcounter < HWHOLE-12'b1) ? vcounter :
                (vcounter < VWHOLE-12'b1) ? vcounter+11'b1 : WZERO;
        end
    end
endmodule
