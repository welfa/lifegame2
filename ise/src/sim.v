`timescale 1ns / 1ps
`default_nettype none

//iverilog用XSTで使えるグローバルバッファ
module BUFG(
    output  wire    O,
    input   wire    I);
assign O = I;
endmodule
