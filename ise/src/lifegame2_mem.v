`timescale 1ns / 1ps
`default_nettype none

//TOP module
module LIFEGAME2_MEM #(parameter WIDTH=12'd400, HEIGHT=12'd300) (
    input   wire        clk,
    input   wire        reset_sw_n,
    input   wire [5:0]  sw,
    //vga I/F
    output  wire        vga_r,  vga_g,  vga_b,  //  VGA RGB
    output  wire        vga_hs, vga_vs          //  VGA H/V sync
);

wire        clk_sram;
wire        dbo;
wire [3:0]  vr, vg, vb;
assign
    vga_r = vr[0],
    vga_g = vg[0],
    vga_b = vb[0];
wire [17:0] sram_addr;
wire [15:0] sram_to, sram_from;
wire        sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n;


LIFEGAME2 #(WIDTH,HEIGHT) lg2(
    clk, clk_sram, reset_sw_n, sw, vr, vg, vb, vga_hs, vga_vs, dbo,
    sram_addr,
    sram_to,
    sram_from,
    sram_we_n, sram_oe_n,
    sram_ub_n, sram_lb_n, sram_ce_n);

wire [16-1:0]   to_bram, from_bram;
assign to_bram = sram_to, sram_from = from_bram;
BRAM bram(clk_sram, ~reset_sw_n, ~sram_we_n, sram_addr, to_bram, from_bram);

endmodule
