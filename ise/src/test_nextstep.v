`timescale 1ns / 1ps
`default_nettype none

module TEST_NEXTSTEP #(parameter WIDTH=8);

reg [WIDTH-1:0]
    top,
    mid,
    btm;
wire [WIDTH-1:0]
    rslt;
NEXTSTEP #(WIDTH) ns(top,mid,btm, rslt);

initial begin
    #2
    top <= 8'b01100;
    mid <= 8'b10010;
    btm <= 8'b10010;
    #1
    $display("%b", rslt);
    $finish;
end
endmodule

