`timescale 1ns / 1ps
`default_nettype none

module TEST_LINECACHE #(parameter
    WIDTH=12'd032, HEIGHT=12'd006, ADDRWIDTH=18,DATAWIDTH=16) ();
//SRAM
wire    [17:0]  sram_addr;
wire    [15:0]  sram_dq;
wire            sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n;
SRAM sram(
    sram_addr,
    sram_dq,
    sram_we_n,
    sram_oe_n,
    sram_ub_n,
    sram_lb_n,
    sram_ce_n);
//
reg clk=1'b1, reset=1'b0;
wire start, rw;
wire [ADDRWIDTH-1:0]    addr_in;
wire [DATAWIDTH-1:0]    data_read;
wire [DATAWIDTH-1:0]    data_write;
SRAMCTRL #(ADDRWIDTH, DATAWIDTH) sramctrl(
    //inner I/F
    ~clk,       reset,
    start,      rw,
    addr_in,
    data_read,
    data_write,
    //SRAM I/F
    sram_addr,
    sram_we_n,      sram_oe_n,
    sram_ce_n,      sram_ub_n,      sram_lb_n,
    sram_dq
);
wire [ADDRWIDTH-1:0]    lg_R_address;
wire [DATAWIDTH-1:0]    lg_R_ram_data;
wire                    lg_R_data_req,  lg_R_data_rdy;
SRAMPROXY #(ADDRWIDTH, DATAWIDTH) sramproxy(
    clk, reset,
    lg_R_address,
    lg_R_data_req,
    lg_R_data_rdy,
    lg_R_ram_data,
    ,
    1'b0,   //reqはrdyが上がった瞬間下げろ
    ,
    ,
    ,
    1'b0,   //reqはrdyが上がった瞬間下げろ
    ,
    ,
    //SRAM I/F
    start,
    rw,
    addr_in,
    data_read,
    data_write);
//
reg  [11:0] lg_hcounter,// = RESETHVC,
            lg_vcounter;// = RESETHVC;
reg page = 1'b0;
wire [WIDTH-1:0]
    lg_top, lg_mid, lg_btm,
    lg_next;
wire    lg_linerdy;
LINECACHE #(WIDTH, HEIGHT, DATAWIDTH) lg_cache(
    clk, reset,
    //lifegame I/F
    page,
    lg_vcounter,
    lg_top, lg_mid, lg_btm,  //まずは1bitだけ
    lg_linerdy,
    //SRAM I/F
    lg_R_address,
    lg_R_data_req,
    lg_R_data_rdy,
    lg_R_ram_data);

always #5 begin
    clk <= ~clk;
end
task COUNTUPSHOW(); begin
    @(posedge clk);
    @(posedge lg_linerdy);
    $display("%d:", lg_vcounter);
    $display("top:%b", lg_top);
    $display("mid:%b", lg_mid);
    $display("btm:%b", lg_btm);
    lg_vcounter = lg_vcounter<HEIGHT-1 ? lg_vcounter + 1 : 0;
end
endtask
initial begin
    lg_hcounter = 0;
    lg_vcounter = HEIGHT-1;
    reset = 0;
    #2
    reset = 1;
    #2
    reset = 0;
    $readmemb("pattern.bin", sram.mem);
    #2
    COUNTUPSHOW();
    COUNTUPSHOW();
    COUNTUPSHOW();
    COUNTUPSHOW();
    COUNTUPSHOW();
    COUNTUPSHOW();
    $finish;

end

endmodule
